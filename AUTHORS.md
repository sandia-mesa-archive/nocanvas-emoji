# Authors

NoCanvas Emoji is available on Sandia Mesa's self-hosted [Gitea](https://code.sandiamesa.com/sandiamesa/nocanvas-emoji) and is provided thanks to the work of the following contributors:

* Sean King - <sean.king@sandiamesa.com>