<?php
	/**
	 *
	 * This includes the functions for the admin menus.
	 *
	 * @package NoCanvas_Emoji
	 * @since 1.0
	 */

	// Register the settings
	function register_nocanvas_emoji_settings() {
		$args = array(
			'default' => 'system_default',
		);
		add_settings_field( 'emoji_style', 'Emoji Style', 'emoji_style_display', 'reading' );
		register_setting( 'reading', 'emoji_style', $args ); 
	} 
	add_action( 'admin_init', 'register_nocanvas_emoji_settings' );

	// Menu callback functions
	function nocanvas_emoji_reading_menu() {
		do_settings_sections( 'nocanvas_emoji' );
	}

	// Menu render functions
	function emoji_style_display() { ?>
		<p><label>
			<input type="radio" name="emoji_style" value="system_default" <?php checked('system_default', get_option('emoji_style'), true); ?>>
			<?php _e( 'System default', 'nocanvas_emoji' ); ?>
		</label></p>
		<p><label>
			<input type="radio" name="emoji_style" value="twemoji" <?php checked('twemoji', get_option('emoji_style'), true); ?>>
			<?php _e( 'Twemoji', 'nocanvas_emoji' ); ?>
		</label><button class="nocanvas-button" type="button"><?php downloadButtonDisplay(); ?></button></p>
		<p><label>
			<input type="radio" name="emoji_style" value="blobmoji" <?php checked('blobmoji', get_option('emoji_style'), true); ?>>
			<?php _e( 'Blobmoji', 'nocanvas_emoji' ); ?>
		</label><button class="nocanvas-button" type="button"><?php downloadButtonDisplay(); ?></button></p>
		<p><label>
			<input type="radio" name="emoji_style" value="noto-emoji" <?php checked('noto-emoji', get_option('emoji_style'), true); ?>>
			<?php _e( 'Noto Emoji', 'nocanvas_emoji' ); ?>
		</label><button class="nocanvas-button" type="button"><?php downloadButtonDisplay(); ?></button></p>
		<p class="description">
			<?php _e( 'You&#39;ll need to download these emoji sets to your ', 'nocanvas_emoji' );
				if ( function_exists( 'classicpress_version' ) ) {
					_e( 'ClassicPress', 'nocanvas_emoji' );
				} else {
					_e( 'WordPress', 'nocanvas_emoji' );
				}
				if ( is_multisite() ) {
					_e( ' Multisite', 'nocanvas_emoji' );
				}
				_e( ' installation in order to use them.', 'nocanvas_emoji' ); ?>
		</p>
	
	<?php }