<?php
	/**
	 *
	 * This indexes the includes for the downloader.
	 *
	 * @package NoCanvas_Emoji
	 * @since 1.0
	 */
	require_once( dirname(__FILE__) . '/ajax.php' );
	require_once( dirname(__FILE__) . '/functions.php' );

    function noCanvasEmojiAdminIncludes() {
		$currentScreen = get_current_screen();

		if ( 'options-reading' === $currentScreen->id ) {
		
			function noCanvasEmojiAjaxScript() {
				wp_register_script( 'noCanvasEmojiAjax', plugins_url( 'ajax.js', __FILE__ ), array('jquery'), false, true );

				// Localize the script with new data
    			$script_data_array = array(
        			'ajaxurl' => admin_url( 'admin-ajax.php' ),
    			);

				wp_localize_script( 'noCanvasEmojiAjax', 'noCanvasEmojiObj', $script_data_array );

				wp_enqueue_script( 'noCanvasEmojiAjax' );
			}
			add_action( 'admin_enqueue_scripts', 'noCanvasEmojiAjaxScript' );
		}
	}
	add_action( 'current_screen', 'NoCanvasEmojiAdminIncludes' );