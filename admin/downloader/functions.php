<?php
	/**
	 *
	 * This contains all the functions related to the emoji set downloader.
	 *
	 * @package NoCanvas_Emoji
	 * @since 1.0
	 */

	// This function returns a JSON response
	function sendResponse( $responseCode ) {
		$response = array('code' => $responseCode, 'data' => '');
		wp_die(json_encode($response));
	}

	// Test AJAX function
	function noCanvasEmoji_testAjaxCallback() {
		sendResponse(1);
	}

	// This function returns a string for the Download button based on whether the emoji set is installed or not.
	function downloadButtonDisplay() {
		_e( 'Download', 'nocanvas_emoji' );
	}