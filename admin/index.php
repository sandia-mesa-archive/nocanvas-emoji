<?php
	/**
	 *
	 * This indexes the includes for the admin menus.
	 *
	 * @package NoCanvas_Emoji
	 * @since 1.0
	 */
    	 
	// Includes
	require_once( dirname(__FILE__) . '/settings.php' );
	require_once( dirname(__FILE__) . '/downloader/index.php' );