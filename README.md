# NoCanvas Emoji
> ⚠️ This project is still a work-in-progress and is not yet ready for production use.

NoCanvas Emoji is a simple WordPress/ClassicPress plugin that disables the HTML canvas fingerprinting method for emojis and renders the emoji with a different method.

## Copyright

Copyright (C) 2020 Sandia Mesa Animation Studios and other contributors (see [AUTHORS.md](AUTHORS.md))<br>
NoCanvas Emoji is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.