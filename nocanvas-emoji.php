<?php
	/**
	 *
	 * Plugin Name: NoCanvas Emoji
	 * Plugin URI: https://code.sandiamesa.com/sandiamesa/nocanvas-emoji
	 * Description: A simple WordPress/ClassicPress plugin that disables the HTML canvas fingerprinting method for emojis and renders the emoji with a different method.
	 * Version: 1.0
	 * Requires at least: TBD
	 * Requires PHP: TBD
	 * Author: Sandia Mesa Animation Studios
	 * Author URI: https://sandiamesa.com
	 * License: GPL v3 or later
	 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
	 * Text Domain: nocanvas-emoji
	 */
   
	// You shall not access me directly.
	if (!defined( 'ABSPATH' )) {
		die();
	}

	// Include the admin folder
	if ( is_admin() ) {
		require_once( dirname(__FILE__) . '/admin/index.php');
	}

	// Begin with the basics. Disable the built-in emoji.
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_styles', 'print_emoji_styles');
	add_filter('emoji_svg_url', '__return_false');